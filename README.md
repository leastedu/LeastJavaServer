# LeastJavaServer

#### 介绍
相比写过java的伙伴都知道，现有的java框架几乎都是重量级的，封装得严严实实的， Spring、Struts、Hibernate等几乎都是重量级的框架，殊不知java只需要不到40行代码就可以实现一个微型服务器的搭建！话不多说，直接上代码：

```
import java.io.*;
import java.net.*;
import java.util.*;
public class LeastServer {
    public static void main(String[] args)throws Exception {
        String requestMessageLine;
        String fileName;
        ServerSocket listensocket = new ServerSocket(6789);
        Socket connectionSocket = listensocket.accept();
        BufferedReader inFormClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        requestMessageLine =inFormClient.readLine();
        StringTokenizer tokenizerdLine=new StringTokenizer(requestMessageLine);
        if(tokenizerdLine.nextToken().equals("GET")){
            fileName=tokenizerdLine.nextToken();
            if (fileName.startsWith("/")){
                fileName=fileName.substring(1);
            }
            File file=new File(fileName);
            int numbOfBytes=(int)file.length();
            FileInputStream inFile=new FileInputStream(fileName);
            byte[] fileInBytes=new byte[numbOfBytes];
            inFile.read(fileInBytes);
            outToClient.writeBytes("HTTP/1.0 200 Document Folllows\r\n");
            if (fileName.endsWith(".jpg")){
                outToClient.writeBytes("Content-Type:image/jpeg\r\n");
            }
            if(fileName.endsWith(".gif")){
                outToClient.writeBytes("Content-Type:image/gif\r\n");
            }
            outToClient.writeBytes("Content-Length:"+numbOfBytes+"\r\n");
            outToClient.writeBytes("\r\n");
            outToClient.write(fileInBytes,0,numbOfBytes);
            connectionSocket.close();
        }
        else System.out.println("Bad Request Message");
    }
}
```

只需要运行如下命令：

javac LeastServer.java

java LeastServer

服务器已经启动了，只需要新建一个test.html:
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>hello,leastedu最简单服务器！</title>
</head>
<body>
<p>
    hello,leastedu最简单服务器！
</p>
</body>
</html>
```
在浏览器里面输入http://127.0.0.1:6789/test.html 

效果如下：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1120/170328_faa79c38_5480936.png "2.png")

看吧，全网最简单的java服务器诞生了！ 