//
// Least Edu ----Learn It Easier!
// @project:   https://gitee.com/leastedu
// @web:       https://blog.csdn.net/leastedu
// @email:     leastedu@sina.com

import java.io.*;
import java.net.*;
import java.util.*;

public class LeastServer {
    public static void main(String[] args) throws Exception {
        String requestMessageLine;
        String fileName;
        ServerSocket listensocket = new ServerSocket(6789);
        Socket connectionSocket = listensocket.accept();
        BufferedReader inFormClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        requestMessageLine = inFormClient.readLine();
        StringTokenizer tokenizerdLine = new StringTokenizer(requestMessageLine);
        if (tokenizerdLine.nextToken().equals("GET")) {
            fileName = tokenizerdLine.nextToken();
            if (fileName.startsWith("/")) {
                fileName = fileName.substring(1);
            }
            File file = new File(fileName);
            int numbOfBytes = (int) file.length();
            FileInputStream inFile = new FileInputStream(fileName);
            byte[] fileInBytes = new byte[numbOfBytes];
            inFile.read(fileInBytes);
            outToClient.writeBytes("HTTP/1.0 200 Document Folllows\r\n");
            if (fileName.endsWith(".jpg")) {
                outToClient.writeBytes("Content-Type:image/jpeg\r\n");
            }
            if (fileName.endsWith(".gif")) {
                outToClient.writeBytes("Content-Type:image/gif\r\n");
            }
            outToClient.writeBytes("Content-Length:" + numbOfBytes + "\r\n");
            outToClient.writeBytes("\r\n");
            outToClient.write(fileInBytes, 0, numbOfBytes);
            connectionSocket.close();
        } else System.out.println("Bad Request Message");
    }
}